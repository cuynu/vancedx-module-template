# Vanced+ Module Template

# DEPRECATED 
- We are now using CorePatch to disable signature verification, and override original YouTube with patched one!

Based on [j-hc module template](https://github.com/j-hc/revanced-magisk-module/tree/main/scripts)

By default, after Vanced+ patching process, CLI will generate patched Vanced+ apk file to `/sdcard/VancedX-APK` (or `/home/username/VancedX-APK`) folder but unlike ReVanced, their CLI will directly mount patched apk to YouTube app meanwhile our Vanced+ not. To mount stock YouTube app to YouTube Vanced+, you "may" need to use this module template and flash it with Magisk/KernelSU/APatch.

**CAUTION : Phh-SU/Superuser/SuperSU are NOT SUPPORTED !!!!** 

## TO DO
- [ ] Prevent module from installing when ZygiskNext 0.9.2+ module installed which is proprietary and untrustable
- [ ] Prevent module from installing when Shamiko module installed which is proprietary and untrustable

## For YouTube Vanced+

Replace `base.apk` on `youtube` folder with original YouTube apk 

Replace `base-vx.apk` with YouTube Vanced+ apk that patched without `vx-microg-support` and `custom-pkgname` patch (version must be match with original YouTube apk)

Replace `version` on `module.prop` to match with original YouTube apk and YouTube Vanced+ apk (`versionCode` leave default !)

Rezip archive, then flash with Magisk or KernelSU/APatch

## For YouTube Music Vanced+ 

Replace `base.apk` on `youtube-music` folder with original YouTube Music apk 

Replace `base-vx.apk` with YouTube Music Vanced+ apk that patched without `vx-microg-support` and `custom-pkgname` patch (version must be match with original YouTube Music apk)

Replace `version` on `module.prop` to match with original YouTube Music apk and YouTube Music Vanced+ apk (`versionCode` leave default !)

Rezip archive, then flash with Magisk or KernelSU/APatch

## Troubleshoot

**Mounted app crashing on device with external sdcard as adoptable storage**

- Disable `Force allow app on external` and uninstall current app that crashing then try re-install module again. Mount usually doesn't work with apps that installed to external sdcard

# Credits 
The icon of Aria inside Vanced+ icon was created by [@sevenc_nanashi@voskey.icalo.net](https://voskey.icalo.net/@sevenc_nanashi) and is licensed under [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/).



