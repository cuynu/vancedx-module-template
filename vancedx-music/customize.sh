#!/system/bin/sh

# Checking for installation environment
# Abort TWRP installation with error message when user tries to install this module in TWRP

if [ $BOOTMODE = false ]; then
	ui_print "ERROR: TWRP based recovery detected"
	ui_print "ERROR: This module can't be installed through recovery mode"
	ui_print "ERROR: Please reboot to Android and install with Magisk/KernelSU !"
	abort "ERROR: Aborting..."
fi


PKGNAME=com.google.android.apps.youtube.music
APPNAME="YouTube Music"

# Unmount YouTube Music app
stock_path=$( pm path $PKGNAME | grep base | sed 's/package://g' )
if [[ '$stock_path' ]] ; then umount -l $stock_path; fi

# Install YouTube Music
ui_print "INFO : Installing stock YouTube Music app..."
SESSION=$(pm install-create -r | grep -oE '[0-9]+')
APKS="$(ls $MODPATH/youtube-music)"
for APK in $APKS; do
pm install-write $SESSION $APK $MODPATH/youtube-music/$APK > /dev/null
done
pm install-commit $SESSION
rm -rf $MODPATH/youtube-music

if ! dumpsys package $PKGNAME | grep path > /dev/null 2>&1
then
	ui_print "ERROR : $APPNAME app can't be installed"
	ui_print "INFO : Try install $APPNAME app manually !"
	abort "ERROR : Aborting..."
fi


STOCKAPPVER=$(dumpsys package $PKGNAME | grep versionName | cut -d= -f 2 | sed -n '1p')
VANCEDXAPPVER=$(grep version= module.prop | sed 's/version=v//')
ui_print "INFO : Installed $APPNAME version : $STOCKAPPVER"
ui_print "INFO : YouTube Vanced+ Music version : $VANCEDXAPPVER"

if [ "$STOCKAPPVER" != "$VANCEDXAPPVER" ]
then
	ui_print "INFO : Installed $APPNAME version : $STOCKAPPVER"
	ui_print "INFO : YouTube Vanced+ Music version : $VANCEDXAPPVER"
	ui_print "ERROR : Installed $APPNAME version are mismatch with Vanced+ !"
	ui_print "INFO : Uninstall current installed $APPNAME app or"
	ui_print "INFO : Find Vanced+ module that match with current installed $APPNAME version !"
	abort "INFO : Aborting..."

fi

ui_print "INFO : Unmounting previous mounts..."

grep $PKGNAME /proc/mounts | while read -r LINE
do 
	echo "$LINE" | grep "$PKGNAME" | cut -d " " -f 2 | sed "s/apk.*/apk/" | xargs -r umount -l
done
sleep 2
ui_print "INFO : Copying necessary files..."
mkdir -p "$MODPATH/app"
rm $MODPATH/app/"$APPNAME"*
mv $MODPATH/*.apk $MODPATH/app/"$APPNAME"vancedx-"$VANCEDXAPPVER".apk
STOCKAPK=$(pm path $PKGNAME | grep base | cut -d ":" -f2)
VANCEDXAPK=$MODPATH/app/"$APPNAME"vancedx-"$VANCEDXAPPVER".apk
ui_print "INFO : Setting permissions..."
chmod 644 "$VANCEDXAPK"
chown system:system "$VANCEDXAPK"
chcon u:object_r:apk_data_file:s0 "$VANCEDXAPK"
sleep 2
ui_print "INFO : Mounting YouTube Vanced+ Music to YouTube Music app..."
mount -o bind "$VANCEDXAPK" "$STOCKAPK"
sleep 2
ui_print "INFO : Force closing current YouTube Music app..."
am force-stop "$PKGNAME"
sleep 2
ui_print "INFO : YouTube Music Vanced+ Installed !"
ui_print "TIPS : To prevent update from Play Store, use zygisk-detach module !"
ui_print "TIPS : GitHub : https://github.com/j-hc/zygisk-detach"
ui_print "TIPS : or try disable update for $APPNAME on Play Store."
