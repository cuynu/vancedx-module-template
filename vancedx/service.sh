#!/system/bin/sh

# Wait till device boot process completes
while [ "$(getprop sys.boot_completed)" != "1" ]; do
	sleep 1
done

# Device online functions
wait_until_login()
{
    # whether in lock screen, tested on Android 7.1 & 10.0
    # in case of other magisk module remounting /data as RW
    while [ "$(dumpsys window policy | grep mInputRestricted=true)" != "" ]; do
        sleep 2
    done
    # we doesn't have the permission to rw "/sdcard" before the user unlocks the screen
    while [ ! -d "/sdcard/Android" ]; do
        sleep 2
    done
}

# Detect temproot
if [ -e /data/local/tmp/magisk ]; then
  sleep 20
else
  sleep 3
fi

# Mounting
wait_until_login
PKGNAME=com.google.android.youtube
STOCKAPPVER=$(dumpsys package $PKGNAME | grep versionName | cut -d "=" -f 2 | sed -n '1p')
VANCEDXAPPVER=$(basename /data/adb/modules/ytvancedx/app/vancedx* .apk | cut -d "-" -f 2)
if [ "$STOCKAPPVER" = "$VANCEDXAPPVER" ]
then
	STOCKAPK=$(pm path $PKGNAME | grep base | cut -d ":" -f2)
	VANCEDXAPK="/data/adb/modules/ytvancedx/app/vancedx-$VANCEDXAPPVER.apk"
	chcon u:object_r:apk_data_file:s0 "$VANCEDXAPK"
	mount -o bind "$VANCEDXAPK" "$STOCKAPK"
fi
am force-stop com.google.android.youtube
